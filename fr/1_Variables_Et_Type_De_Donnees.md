# Les variables et types de données
Comme dans la plupart des langages de programmation, en Ruby on assigne et on utilise des variables qui contiennent des données.

En Ruby, il existe plusieurs types de variables. Il existe également plusieurs types de données. Nous allons donc commencer par voir les variables puis nous verrons les données.

## Les variables
Une variable est un élément du programme qui associe un nom (identifiant) à une valeur. La valeur associé par ce nom peut changer au cours du temps, d'où le nom de variable. Selon le contexte, un même nom peut également être associé à une valeur différente, tout dépend de la portée de cette variable.

### Les différente portée de variable
En Ruby, la portée d'une variable est explicité par son nom : c'est les premiers caractères qui indiquent la porté.

Voici les différentes portées :
* **Variables globales**

    Leur nom commence par le signe `$` et contient n'importe quel autre caractère non-syntaxique.

    Ces variable sont accessible partout et peuvent être modifiée partout. Du fait de leur portée, accéder à une de ces variable lorsqu'elle n'a pas été assigné renvoie `nil` (l'objet néant).

* **Variables d'instance**

    À la différence des variables globales, le nom des variables d'instance commence par le signe `@`.

    Ces variables ne sont accessible que dans le contexte d'un objet, ça veut dire que d'un objet à l'autre la valeur associé à cette variable peut être différente. Ces variables sont donc utilisées pour stocker des informations spécifiques sur des objets.

* **Variables de classe**

    Ces variables ont pour vocation de stocker des informations spécifiques à une classe d'objet, du coup, elle commencent par deux signe `@`. Il ne faut pas les confondre avec les variables d'instance car contrairement aux variables d'instance, il n'est pas possible d'accéder à une variable de classe non initialisé.

    La portée d'une variable de classe est un peu spécifique. Dans le contexte de la classe qui a définit cette variable ainsi que celui de ses classes enfant, la variable est accessible. Par contre, dans le contexte des classes parente cette variable est inaccessible.

    Ceci peut causer beaucoup de soucis dans la définition des méthodes qui accèdent à ces variables, c'est pourquoi il faut le plus possible éviter ces variables.

* **Constantes**

    Bien que le nom de ce type de variable est "Constante" ce type de variable décrit bien des variables. Le nom de ces variable commence toujours par une majuscule. Depuis Ruby 2.6 ça peut être une majuscule non latine (par exemple une lettre grecque en majuscule).

    La portée de ces variables est un peu similaire à celle des variables de classe à la différence qu'entre les différentes classes enfante ces variable peuvent être associée à une valeur différente.

    J'attire également votre attention sur l'accès à ces variable : Quand vous écrivez le nom d'une constante dans votre code (une méthode), la constante qui sera utilisée n'est pas celle du contexte de l'objet en priorité, c'est celle du contexte de la définition de la méthode. Ainsi, si une méthode a été hérité d'une classe parente et que la constante a une valeur différente dans la classe actuelle, la valeur lue par la méthode est celle de la constante dans la classe parente.

    Cela dit, il existe des manière d'accéder aux constantes qui permette d'être sûr à 100% qu'on accède à la bonne constante.

* **Les variables locales**

    Les variables locales ont un nom qui commence par n'importe quel caractère qui ne définit pas l'un des précédent types de variable. Notez que le caractère `_` ou l'espace insécable ne sont pas des caractères syntaxique, de ce fait, ils peuvent être utilisés pour nommer des variable locales. (Voir [Un script pas drôle](../scripts/not_a_funny_script.rb))

    La portée de ces variable comme leur nom l'indique est locale. Si le contexte change, la valeur associée à ces variable change, si toute fois la variable a été définie.

    Notez que les méthodes se nomment de la même manière que les variables locale, ceci peut être parfois un peu confusant.

## Les types de données
En Ruby il existe plusieurs types de données qui permettent certaines manipulations.

### Les nombres
Les nombres sont des données de type Numeric, on y retrouve les entiers (Integer), les flottants (Float), les rationnels (Rational) et les complexes (Complex).

Il existe plusieurs moyen de définir ces nombre ou de convertir des objets en ces nombres.

#### Les entiers

Les entiers s'écrivent de différente manière :
* Si c'est un entier binaire on l'écrit ainsi : `0b<nombre>` où `<nombre>` est remplacé par une suite de digit binaires.
* Si c'est un entier octal on l'écrit ainsi : `0<nombre>` où `<nombre>` est remplacé par une suite de digit octaux.
* Si c'est un entier hexadécimal on l'écrit ainsi : `0x<nombre>` où `<nombre>` est remplacé par une suite de digit hexadécimaux.
* Sinon on écrit une suite de digit décimaux.

Les nombres entiers peuvent être séparés par un underscore ( `_` ) pour séparer les milliers ou des paquets de nombres.

Exemples :
```ruby
0xDEADBEEF # 3735928559
0b1011_0010 # 178
0755 # 493 or rwxrw-rw-
1_000_000
```

#### Les flotants

Les nombres flotants s'écrivent soit avec un point pour séparer la partie entière de la partie flotante, soit à l'aide du caractère `e` pour définir l'exposant du nombre. Les deux écritures peuvent être combinées.

Exemples :
```ruby
1e2 # 100.0
0.5
0.314e1 # 3.14
```

#### Les complexes

Les nombres complexes s'expriment de la même manière qu'en maths, on termine le nombre par un `i` et ça en fait un complexe.

Exemples :
```ruby
5i # (0+5i)
3 + 2i # (3+2i)
1 + 0.33i # (1+0.33i)
0.14e3i # (0+140.0i)
```

#### Les rationnels

Les nombres rationnels s'expriment à l'aide du symbole `r` à la fin du nombre. Lorsque vous définissez un complexe, le `r` vient avant le `i`. 

Notez qu'il n'est pas possible d'utiliser la notation exposant (`e`) avec la notation rationnelle (`r`).

Exemples :
```ruby
5r # (5/1)
0.33r # (33/100)
0.3ri # (0+(3/10)*i)
```

#### La conversion des nombres
On a vu comment écrire des nombres, maintenant il serait pratique de pouvoir convertir ces nombres en d'autres types de nombres.

* Convertir en entier : nombre.to_i
* Convertir en flottant : nombre.to_f
* Convertir en complexe : nombre.to_c
* Convertir en rationnel : nombre.to_r

Exemples :

```ruby
3.14.to_i # 3
10.to_f # 10.0
1.to_c # (1+0i)
5.to_r # (5/1)
```

Note importante : Les complexe ne peuvent pas être convertis en rationnels (je ne sais pas pourquoi) mais les rationnels peuvent être convertis en complexe.

#### Les opérations sur les nombres

Les nombres peuvent subir plusieurs type d'opérations : 
* L'addition : `a + b`
* La soustraction : `a - b`
* L'opposé : `-a`
* La multiplication : `a * b`
* La mise à la puissance : `a**n`
* La division : `a / b`
* Le reste de la division : `a % b`

    Attention, les nombres complexe ne supportent pas le reste de la division.

Pour les nombres entier il existe des opérations logiques :
* Le et logique bit à bit : `a & b`
* Le ou logique bit à bit : `a | b`
* Le ou-exclusif logique bit à bit : `a ^ b`
* L'inversion des bits : `~a`

### Les chaînes de caractères

Les chaînes de caractères permettent de décrire du texte. Leur utilisation principale est le traitement de fichier et l'affichage de message à l'utilisateur. Au sein du programme, les chaînes de caractères ne doivent pas servir à indexer des éléments car le traitement de celles-ci est long et fastidieux.

Il existe plusieurs manière d'écrire des chaînes de caractères :

* Les chaînes simples : `'ma chaîne'`

    Cette écriture de chaîne de caractère permet d'écrire des chaînes qui ne subissent aucun traitement particulier (caractère d'échappement). Le seul caractère d'échappement accepté dans ces chaînes est `\'` car l'apostrophe est caractère qui délimite ce genre de chaîne.

* Les chaînes simples pourcent : `%q(ma chaîne)`

    Ces chaînes sont équivalente aux chaînes simples, elle permettent juste d'utiliser l'apostrophe et le guillemet en même temps.

* Les chaînes avancées : `"Ma chaîne"`

    Ce type de chaîne permet l'interpolation et les caractères d'échappement. C'est à utiliser seulement si vous avez besoin de ces mécanismes.

* Les chaînes avancées pourcent : `%Q(ma chaîne)`

    Ce type de chaîne fonctionne comme les simple pourcent mais en autorisant l'interpolation et les caractères d'échappement.

Exemples : 

```ruby
'Chaîne simple #{0}\n' # "Chaîne simple \#{0}\\n"
%q("c'est aussi simple #{0}\n") # "\"c'est aussi simple \#{0}\\n\""
"Ca c'est avancé #{0}\n" # "Ca c'est avancé 0\n"
%Q("c'est aussi avancé #{0}\n") # "\"c'est aussi avancé 0\n\""
```

### Les symboles

Les symboles sont un outil magique de Ruby qui sert à indexer des éléments ou à retrouver d'autres éléments. Les symboles sont immuable, ça veut dire que leur id d'objet est toujours le même (contrairement aux chaînes de caractère), ainsi, Ruby s'en sert pour identifier les méthodes, les constantes, les variables d'instance et plein d'autre choses.

Pour écrire un symbole il suffit de commencer par le caractère `:`. Ensuite il peut y avoir des cas qui requièrent une écriture particulière.

Exemples :
```ruby
:nom_method # Symbole d'un potentiel nom de méthode
:@variable_dinstance # Symbole d'un potentiel nom de variable d'instance
:NOM_CONSTANTE # Symbole d'un potentiel nom de constante
:"nom avec espace" # Symbole qui doit rarement être utilisé mais dont le nom contient des caractères qui nécessite de délimiter le symbole.
```

Note : Les chaînes de caractère peuvent être converties en nombre.

Pour convertir n'importe quoi en chaîne de caractère, il suffit d'utiliser la méthode `to_s`.

### Les tableaux ordonnés

Les tableaux ordonnés permettent de stocker des objets dans un espace linéaire indexé par des nombres (commençant par 0). Ces tableaux s'écrivent à l'aide de crochet et leurs éléments sont séparés par des virgules. Ils contiennent également tout un tas de méthode de manipulation que nous verrons plus tard.

Exemples :

```ruby
[] # Tableau vide
[0, '1', :deux] # Tableau contenant trois éléments, les types de donnée sont arbitraire.
[[1, 2], [3, 4]] # Tableau contenant des tableaux
```

### Les tableaux associatifs

Les tableaux associatifs sont des tableaux qui associent un objet clé à un autre objet valeur. La plupart du temps ces tableaux utilisent des symboles pour indexer leur valeur (paramètre optionnels nommés de méthode). Toute fois, il existe des **mauvais programmeurs** qui utilisent volontairement des chaînes de caractère pour indexer leur tableau associatif.

L'usage d'autre objets que des Symboles pour indexer les valeurs d'un tableau associatif doit être justifié (Table de conversion, limitation d'un langage de communication comme JSON etc...).

L'écriture des tableau associatifs se font à l'aide d'accolades, si vous utilisez des symboles, mettez le `:` à la fin au lieu du début pour indiquer la limitation entre la clé et la valeur. Si vous utilisez des objets autre (nombres par exemple) utilisez `=>` pour délimiter la clé et la valeur.

Exemples :

```ruby
{} # Tableau associatif vide
{ nom: "Yuri", age: 25, nationalite: :Français } # Tableau contenant des informations
{ 1 => "Janvier", 2 => "Février" } # Tableau de conversion du numéro de mois en nom du mois
```

### Les booléens

Contrairement à d'autres langage, Ruby possède des booléens et ceux-ci **ne sont pas** équivalent à des nombres. Les booléens sont :
* `true` quand quelque chose est vrai.
* `false` quand quelque chose est faux.

Pour Ruby `0` n'est pas équivalent à `false`, ne faites pas la confusion.

### Les objets

Les objets sont tous les types de données descendant généralement directement de Object. C'est vos des instance de vos concepts (Point, Dossier, ListeEtudiant etc...).

Nous verrons plus tard comment décrire un concept (une classe) et comment manipuler des objets.