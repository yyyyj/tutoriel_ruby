# Les conditions

En Ruby les conditions fonctionnent d'une manière un peu différente d'autres langages.

Premièrement nous avons deux types de conditions :
* `if` : exécute le code si la condition est valide.
* `unless` exécute le code si la condition n'est pas valide.

La présence de ces deux types de condition permet une grande flexibilité dans la programmation des conditions (surtout celle des conditions de protection). Plutôt que de tout encadré dans un gros `not` on utilise simplement `unless` et `unless` sera beaucoup plus efficace que `if not`.

## Qu'est-ce qu'une condition valide ?

Avant de commencer à écrire des conditions, on va se demander qu'est-ce qu'une condition valide.

Nous avons vu qu'il existe des booléens `true` et `false`. Logiquement, `true` valide une condition pendant que `false` ne la valide pas. Mais ce n'est pas tout.

Dans les fait il existe que deux choses qui ne valident pas une condition : `false` et `nil`. Tout le reste, quelque soit sa nature, validera une condition. Ainsi, `0` validera toujours une condition, `[]` aussi, `''` aussi, etc...

Ceci a un effet positif précis : vérifier rapidement que l'objet existe avant de s'en servir. Si vous voulez détecter quand un tableau est vide ou une chaîne est vide, utilisez la méthode `empty?`.

## Les manières d'écrire des conditions

Il existe deux manières d'écrire des conditions en Ruby.
* En bloc :

    ```ruby
    if condition
      # code
    end
    ```
* En fin de ligne
    ```ruby
    code if condition
    ```

Si une condition est écrite en bloc, il faut toujours la terminer par un `end`. Par contre, si une condition est écrite en fin de ligne, il ne faut pas mettre le `end` puis-ce que la partie exécuté par la condition est ce qui se trouve entre le début de ligne et le `if`. (C'est l'assertion avant le `if` pour être plus précis.)

## Les conditions sinon

Quand vous écrivez une condition en bloc, vous pouvez utiliser `else` qui permet d'exécuter le code si la condition n'a pas été validé.

Avec `if` :
```ruby
if condition
  # Code exécuté si condition est valide
else
  # Code exécuté si condition n'est pas valide
end
``` 

Avec `unless` :
```ruby
unless condition
  # Code exécuté si condition n'est pas valide
else
  # Code exécuté si condition est valide
end
```

Il est souvent recommandé d'écrire les conditions `if` plutôt que `unless`. Comme je suis un peu faignant, j'écris la plus facile des deux quand j'y pense. (Comme je suis également un peu bizarre il m'arrive plus souvent de trouver une condition qui ne valide pas facilement).

## Les conditions sinon si

Ces conditions là ne fonctionnent qu'avec le `if` et permettent de décrire plusieurs conditions. Ces conditions sont `elsif`.

Exemple sans else :
```ruby
if condition
  # Code exécuté si condition valide
elsif autre_condition
  # Code exécuté si autre_condition valide
end
```

On peut également en utiliser autant qu'on veut :
```ruby
if condition
  # Code exécuté si condition valide
elsif autre_condition_1
  # Code exécuté si autre_condition_1 valide
elsif autre_condition_2
  # Code exécuté si autre_condition_2 valide
end
```

On peut également terminer nos conditions par un `else` pour couvrir le cas où aucune condition ne valide :
```ruby
if condition
  # Code exécuté si condition valide
elsif autre_condition_1
  # Code exécuté si autre_condition_1 valide
elsif autre_condition_2
  # Code exécuté si autre_condition_2 valide
else
  # Si aucune condition n'as été valide
end
```

Note : Dans un bloc conditionnel, seul une des conditions est exécuté si l'une d'elle est valide, une fois arrivé à la fin du code exécuté, on saute directement au `end`.

## Il est où le switch case ?

En Ruby nous avons un mécanisme similaire, ça génère vite des conditions monstrueusement longues mais c'est là. Ce mécanisme c'est `case` `when` :

```ruby
case valeur
when autre_valeur
  # Partie exécuté si valeur est équivalent à autre valeur
when autre_valeur2, autre_valeur3, autre_valeur4
  # Partie exécuté si valeur est exécuté si valeur est équivalente à l'une des trois autre_valeur renseigné après le when.
else
  # Partie exécuté si valeur ne correspond à rien.
end
```

En Ruby, on ne met pas deux `when` l'un après l'autre pour vérifier plusieurs égalité on sépare toute les valeurs qui valident par une virgule. En effet, on utilise pas `break` dans un `case` `when`, quand le code a été exécuté on saute directement au `end`.

Exemple :
```ruby
case age
when 0..17 # vérifier 0, 1, 2, 3, 4, ... , 17
  puts 'Tu es mineur.'
when 18
  puts 'Tu es devenu majeur.'
else
  puts 'Tu es majeur.'
end
```

Quand je disais équivalent ça voulait dire que Ruby fait un peu plus qu'une simple vérification d'égalité, quand il y a un Range il teste si la valeur est dans le Range.

## Les conditions ternaire

Ruby comprend aussi les conditions ternaire, elles s'écrivent comme dans tous les autres langages mais suivent les même règles que les autres conditions en Ruby, valide si la valeur n'est pas `false` ou `nil`.

Exemple :
```ruby
majorite_str = (age >= 18 ? 'majeur' : 'mineur')
puts format('Tu es %<majorite>s.', majorite: majorite_str)
# Affichera si age >= 18 :
# Tu es majeur.
# Sinon :
# Tu es mineur.
```
