# Tutoriel de Programmation Ruby
## Introduction

Ce tutoriel vise à apprendre les rudiments de la programmation Ruby sur des versions moderne de Ruby (2.5+). Il existe d'autres tutoriels très bien fait à ce sujet [Tutoriel Video par 
Grafikart.fr](https://www.youtube.com/watch?v=PmVcZK3Jwy4&t=0s&list=PLjwdMgw5TTLVVJHvstDYgqTCao-e-BgA8&index=4) et d'autres que je n'apprécie pas vraiment au vu de leur approche.

Ce tutoriel sera donc une version texte accompagné de divers scripts d'exemple. 

Notez que tout ne sera pas entièrement expliqué, il y a sûrement des méthodes par exemple qui peuvent réaliser la tâche que vous désirez mais qui n'apparaît pas dans ce tutoriel. Je vous conseille de chercher sur [Ruby Doc](https://ruby-doc.org/) ou de simplement lire les pages de certaines classes. Ca peut vous être très utile.

## Table des matières

1. [Les variables et les types de données](1_Variables_Et_Type_De_Donnees.md)
2. [Les tableaux ordonnés](2_Tableaux_Ordonnes.md)
3. [Les tableaux associatifs](3_Tableau_Associatifs.md)
4. [Les chaînes de caractères et expressions régulières](4_Chaines_Et_Regexp.md)
5. [Les conditions](5_Conditions.md)
6. [Les méthodes et les blocs](6_Methodes_Et_Bloc.md)
7. [Les boucles](7_Boucles.md)
8. [Les classes et les objets](8_Classes_Et_Objets.md)
9. [Les modules](9_Modules.md)
10. [La gestion des erreurs](10_La_Gestion_Des_Erreurs.md)